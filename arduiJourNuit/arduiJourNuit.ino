//For Arduino > 1.6.x    need following. Arduono 1.0.X no issue.
//Open the hardware/arduino/avr/platform.txt file in WordPad (Notepad will not work due to the way the file is structured). 
//Turn off "Word wrap" so lines can be counted more easily.
//Find this line, about 16 lines down from the top:
//compiler.c.flags=-c -g -Os -w -ffunction-sections -fdata-sections -MMD
//Change the -Os to -O2 as below:
//compiler.c.flags=-c -g -O2 -w -ffunction-sections -fdata-sections -MMD
//Next find a second line a little further down the file, about 23 lines from the top:
//compiler.cpp.flags=-c -g -Os -w -fno-exceptions -ffunction-sections -fdata-sections -fno-threadsafe-statics -MMD
//Again, change the -Os to -O2 as below:
//compiler.cpp.flags=-c -g -O2 -w -fno-exceptions -ffunction-sections -fdata-sections -fno-threadsafe-statics -MMD
//In practice it is just a case of changing the "s" to a "2". Note that is a letter "O" not a zero in the command line.
//Now save the file, don't worry about any format warning. Next time it will open in Notepad OK!
//or
#pragma GCC optimize ("-O2")


//#define MODE_HCHP_ZONE_A
//#define MODE_HCHP_ZONE_B
//#define MODE_HCHP_ZONE_C
#define MODE_HCHP_ZONE_D       // Cagnes
//#define MODE_HCHP_ZONE_E
//#define MODE_EJP
//#define MODE_TEMPO

//#define SCHEMA_PRISE
#define SCHEMA_DIN


// These constants won't change.  They're used to give names
// to the pins used:
#if defined ( SCHEMA_PRISE )
const int PINA_230V   = A0;  // Analog input pin
const int PIND_RELAY  = 13;  // pin to activate relay
const int PIND_LED_RED1 = 12;
const int PIND_LED_YEL1 = 11;
const int PIND_LED_RED2 = 10;
const int PIND_LED_YEL2 = 9;

#elif defined ( SCHEMA_DIN )

const int PINA_230V   = A0;  // Analog input pin
const int PIND_RELAY  = 10;  // pin to activate relay
const int PIND_LED_RED1 = 11;
const int PIND_LED_YEL1 = 12;

#endif


// Liste des 40 signaux EDF
enum EDF_SIGNAL {
  HC_DT_A  = 1,  /* Heures Creuses du Double Tarfif code A */
  HP_DT_A  = 2,  /* Heures Pleines du Double Tarfif code A */
  HC_DT_BC = 3,  /* Heures Creuses du Double Tarfif code B & C */
  HP_DT_C  = 4,  /* Heures Pleines du Double Tarfif code C */
  ALERTEJP = 5,  /* 5 et pas 15, alerte EJP */
  HP_DT_B  = 6,   /* Heures Pleines du Double Tarfif code B */
  EXTINEP2 = 7,   /* Extinction EP2 */
  HC_DT_D  = 8,   /* Heures Creuses du Double Tarfif code D */
  HP_DT_D  = 9,   /* Heures Pleines du Double Tarfif code D */
  EXTINEP1 = 10,  /* Extinction EP1 */
  HC_DT_E  = 11,  /* Heures Creuses du Double Tarfif code E */
  HP_DT_E  = 12,  /* Heures Pleines du Double Tarfif code E */
  ALLUMEP1 = 13,  /* Allumage EP1 */
  HCVERT   = 14, 
  FINEJP   = 15,
  ALLUMEP2 = 16, /* Allumage EP1 */
  HPVERT   = 17, /* Heures Pleins Tarif Vert */
  DEMBLEU  = 18, /* Demain BLEU */
  RESERVE1 = 19,
  PTEVERT  = 20, /* Pointe Tarif Vert */
  DEMBLANC = 21, /* Demain BLANC */
  RESERVE2 = 22,
  HCROUGE  = 23, /* Heures Creuses ROUGE */
  DEMROUGE = 24, /* Demain ROUGE */
  RESERVE3 = 25,
  HPROUGE  = 26, /* Heures Pleins ROUGE */
  CHELECES = 27, /* GGF BIP Mise en Service Chaudiere Electrique */
  RESERVE4 = 28, 
  DEMAINXX = 29, /* Pas d'annonce de la couleur du lendemain */
  CHELECHS = 30, /* GGF BIP Arret Chaudiere Electrique */
  RESERVE5 = 31,   
  HCCODEY  = 32, /* Heures Creuses Codage Y */
  BLEU     = 33, /* Jour BLEU */
  RESERVE6 = 34,
  HPCODEY1 = 35, /* Heures Pleines Codage Y */
  BLANC    = 36, /* Jour BLANC */
  RESERVE7 = 37,
  HPCODEY2 = 38, /* Heures Pleines Codage Y */
  ROUGE    = 39, /* Jour ROUGE */
  RESERVE8 = 40,
  MAX_SIGNAL = 40
};


enum SIGNAL_STATE{
  SEQ_STATE_DETECT_DEBUT_SIGNAL_HAUT,
  SEQ_STATE_DETECT_DEBUT_SIGNAL_BAS,
  SEQ_STATE_ANAYLSE_SIGNAL_HAUT,
  SEQ_STATE_ANAYLSE_SIGNAL_BAS,
  SEQ_STATE_SIGNAL_DECODED
};

static int sig175CumulCpt   = 0;
static int sig175CumulState = 0;
static int sig175CumulVal1 = 0;
static int sig175CumulVal2 = 0;
static int compteur200ms = 0;

static unsigned char lastDecodedSignal[MAX_SIGNAL] ={0};
static int newValue = 0;

#define SIG175_COMPT_MAX     7
//320V crete a crete ==> 0,5V a 4,5V 
//2 volt de variation et représenté par environ 0,028V
// 2 x 4.5 / 320 => 0,028V
// 5 V   => 1024
// 0,028 => 5
// Seuil = 5 x 14 acquisition => 80
#define SIG175_CUMUL_SEUIL   100

/* 700Hz => 1,428ms
   50Hz => 20ms
   Un top de 1 seconde suivi d'un silence de 2,75 secondes indique 
   le début de trame. 
   La trame comporte 40 emplacements d'impulsions de 2,5 secondes 
   et sa durée totale est de 102,25 secondes.
   Chaque emplacement d'impulsion se compose d'une durée de 
   1 seconde pendant laquelle il peut y avoir ou non émission de la
   porteuse à 175 Hz, et d'un silence de 1,5 secondes servant de 
   séparateur avec l'emplacement suivant. Si le top est présent, 
   il déclenche dans les récepteurs un basculement.
*/


void evalSeqenceState(int mesure); 


void setup() {
  Serial.begin(115200); 
  
  cli();//stop interrupts
  
  //set timer1 interrupt at 700Hz
  TCCR1A = 0;// set entire TCCR1A register to 0
  TCCR1B = 0;// same for TCCR1B
  TCNT1  = 0;//initialize counter value to 0
  // set compare match register for 700 hz 
  OCR1A = 22848;// = (16*10^6) / (700 * prescaler) - 1 (must be <65536)
  // turn on CTC mode
  TCCR1B |= (1 << WGM12);
  // Set CS10 and CS12 bits for 1 prescaler
  //TCCR1B |= (1 << CS12) | (1 << CS10); //prescaler 1024
  TCCR1B |=  (1 << CS10); //prescaler 1
  
  // enable timer compare interrupt
  TIMSK1 |= (1 << OCIE1A);
  
  
  sei();//allow interrupts
  
  Serial.println("Init done");
  // initialize the digital pin as an output.
  // to avoid glitch, first initialize the output value
  // NOTA, if pin 13, it will not avoid the glitch at boot up.
  digitalWrite(PIND_RELAY, LOW);
  digitalWrite(PIND_LED_RED1, LOW);
  digitalWrite(PIND_LED_YEL1, LOW);
#if defined ( SCHEMA_PRISE )
  digitalWrite(PIND_LED_YEL2, LOW);
  digitalWrite(PIND_LED_RED2, LOW);
#endif
  pinMode( PIND_RELAY, OUTPUT);
  
  pinMode( PIND_LED_RED1, OUTPUT);
  pinMode( PIND_LED_YEL1, OUTPUT);

#if defined ( SCHEMA_PRISE )
  pinMode( PIND_LED_YEL2, OUTPUT);
  pinMode( PIND_LED_RED2, OUTPUT);
#endif

  
  setLed1On();
#if defined ( SCHEMA_PRISE )
  setLed2On();
  delay(1000); /* 1s */
  setLed2Off();
#endif
  delay(1000); /* 1s */
  setLed1Off();
}

ISR(TIMER1_COMPA_vect){//timer1 interrupt
  int anaVal;
  int sig175Mesure = 0;
  
  // gere la partie echantillonage
  if ( sig175CumulCpt < SIG175_COMPT_MAX )
  {
    anaVal = analogRead(PINA_230V);
    switch (sig175CumulState)
    {
      case 0:
        sig175CumulVal1 +=  anaVal;
        sig175CumulState++;
        break;
      case 1:
        sig175CumulVal2 +=  anaVal;
        sig175CumulState++;
        break;
      case 2:
        sig175CumulVal1 -=  anaVal;
        sig175CumulState++;
        break;
      case 3:
        sig175CumulVal2 -=  anaVal;
        sig175CumulState = 0;
        sig175CumulCpt++;
      break;
      default:
        //error
        sig175CumulState = 0;
        sig175CumulCpt = SIG175_COMPT_MAX; //to stop acquisition
        break;
    }
  }
  
  if ( sig175CumulCpt == SIG175_COMPT_MAX )
  {    
    sig175Mesure = abs(sig175CumulVal1) + abs(sig175CumulVal2);
    
    //Serial.print("Val1 = ");
    //Serial.print(sig175CumulVal1);
    //Serial.print("\tVal2 = ");
    //Serial.print(sig175CumulVal2);
    //Serial.print("\tVal3 = ");
    //Serial.print(sig175Mesure);
    //Serial.println();
    
    evalSequenceState(sig175Mesure);  

    sig175CumulCpt++;
  }
  
  //On attend 200ms avant de recommencer une aquisition
  //200ms a 700MHz => 140 interrupt
  compteur200ms++;
  if (compteur200ms >= 140 )
  {
    //restart an acquisition.    
    sig175CumulVal1 = 0;
    sig175CumulVal2 = 0;
    sig175CumulState = 0;
    sig175CumulCpt = 0;
    compteur200ms=0;/* reinit the counter */
  }
}

void setLed1On(void)
{
     digitalWrite(PIND_LED_RED1, HIGH);
     digitalWrite(PIND_LED_YEL1, LOW);
}

void setLed1Off(void)
{
     digitalWrite(PIND_LED_RED1, LOW);
     digitalWrite(PIND_LED_YEL1, HIGH);
}

#if defined ( SCHEMA_PRISE ) 
void setLed2On(void)
{
     digitalWrite(PIND_LED_RED2, HIGH);
     digitalWrite(PIND_LED_YEL2, LOW);
}

void setLed2Off(void)
{
     digitalWrite(PIND_LED_RED2, LOW);
     digitalWrite(PIND_LED_YEL2, HIGH);
}
#endif

void setRelayOn(void)
{
  digitalWrite(PIND_RELAY, HIGH);
}

void setRelayOff(void)
{
  digitalWrite(PIND_RELAY, LOW);
}

void loop() {
  
  if ( newValue == 1)
  {
    newValue = 0;

    //Serial.println("Signal Detected");
    //for ( int i = 0; i < MAX_SIGNAL ; i++ )
    //{
    //  Serial.print("signal ");
    //  Serial.print(i + 1);
    //  Serial.print(" : ");
    //  Serial.println(lastDecodedSignal[i]);
    //}
    
#if defined ( MODE_HCHP_ZONE_D )
    if ( lastDecodedSignal[ HP_DT_D - 1 ] >= 3 )
    {
      setLed1Off();
      setRelayOff();
    }
    if ( lastDecodedSignal[ HC_DT_D - 1 ] >= 3 )
    {
      setLed1On();
      setRelayOn();
    }
#elif defined ( MODE_EJP )
    // 5 seule : Alerte, la veille ;
    if ( ( lastDecodedSignal[ ALERTEJP - 1 ] >= 3 ) && ( lastDecodedSignal[ FINEJP - 1 ] < 3  ) )
    {
      setLed2On();
    }
    //5 et 15 ensemble : Début de jour de pointe ;
    if ( ( lastDecodedSignal[ ALERTEJP - 1 ] >= 3 ) && ( lastDecodedSignal[ FINEJP - 1 ] >= 3  ) )
    {
      setLed1On();
      setLed2Off();
      setRelayOn();
    }
    //15 seule : Fin de jour de pointe.
    if ( ( lastDecodedSignal[ ALERTEJP - 1 ] < 3 ) && ( lastDecodedSignal[ FINEJP - 1 ] >= 3  ) )
    {
      setLed1Off();
      setRelayOff();
    }
#elif defined ( MODE_TEMPO )
    /* 
     * 33 : Tarif BLEU ;
     * 36 : Tarif BLANC ;
     * 39 : Tarif ROUGE ;
     * 32 : Tarif HC des jours BLEUS et BLANCS ;
     * 23 : Tarif HC des jours ROUGES ;
     * 35 : Tarif HP des jours BLEUS et BLANCS ;
     * 26 : Tarif HP des jours ROUGES ;
     * 18 : Tarif du lendemain BLEU ;
     * 21 : Tarif du lendemain BLANC ;
     * 24 : Tarif du lendemain ROUGE ;
     * 29 : Fin du tarif du lendemain ; ( pas d'annonce du lendemain )
     */
     if ( lastDecodedSignal[ ROUGE - 1 ] >= 3 ) )
     {
	setLed2On();
	setRelayOff();
     }
     
     
		   
    
#endif
    
    
  }

}

/*
 *@return the delay to wait before the next acquisition 
 */
void evalSequenceState(int mesure)
{
  static int nbMesures = 0;
  static int nbMesuresPositives = 0;
  static int sequenceState = SEQ_STATE_DETECT_DEBUT_SIGNAL_HAUT;
  static int signalCpt = 0;
  static int debugMesures[8];

  //Serial.println(millis());
  
  switch ( sequenceState )
  {
   case SEQ_STATE_DETECT_DEBUT_SIGNAL_HAUT:
     //1s signal, suivi de 2,75 seconde blanc.
     if ( mesure > SIG175_CUMUL_SEUIL)
     {
       nbMesuresPositives++;
     }
     if (nbMesuresPositives>0)
     {
       debugMesures[nbMesures] = mesure;
       nbMesures++;
     }
     //5 mesures depuis la premiere detection (ie 1s), 
     //vérifie si au moins 3 valeurs positives
     if ( nbMesures >= 5 )
     {
       //timestamps = millis();
       if ( nbMesuresPositives >= 3)
       {
         //a priori debut de signal, on va a l'etape suivante
         //Serial.print(timestamps);
         //Serial.print("Debut ");
         //Serial.print(nbMesuresPositives);
         //Serial.println(" mpos");
         
         //for (int dbg=0;dbg<5;dbg++)
         //{
         //  Serial.print(debugMesures[dbg]);
         //  Serial.print(" ");
         //}
         //Serial.println();
         
         sequenceState = SEQ_STATE_DETECT_DEBUT_SIGNAL_BAS;
         nbMesures = 0;
         nbMesuresPositives = 0;
       }
       else
       {
         //erreur fausse detection
         sequenceState = SEQ_STATE_DETECT_DEBUT_SIGNAL_HAUT;
         nbMesures = 0;
         nbMesuresPositives = 0;
         //Serial.print(timestamps);
         //Serial.println(" Debut Err");
       }
     }
   break;
   case SEQ_STATE_DETECT_DEBUT_SIGNAL_BAS:
     //2.75ms a l'état bas.
     // on admet que le premier et le dernier soit des états hauts, mais pas plus.
       //1s signal, suivi de 2,75 secondes blanc.
     if ( mesure > SIG175_CUMUL_SEUIL)
     {
       nbMesuresPositives++;
     }
     nbMesures++;
     if ( nbMesures >= 13 )  //13 x 200ms = 2600ms
     {
       //timestamps = millis();
       if (nbMesuresPositives <=2 )
       {
         //a priori debut de signal, on va a l'etape suivante
         //Serial.print(timestamps);
         //Serial.print(" Debut2 ");
         //Serial.print(nbMesuresPositives);
         //Serial.println(" mpos");
         
         //delay(149); //manque encore 150ms d'attente
         //retDelay += 149; //manque encore 150ms d'attente
         
         sequenceState = SEQ_STATE_ANAYLSE_SIGNAL_HAUT;
         nbMesures = 0;
         nbMesuresPositives = 0;
         signalCpt = 0;
       }
       else
       {
         //erreur fausse detection
         sequenceState = SEQ_STATE_DETECT_DEBUT_SIGNAL_HAUT;
         nbMesures = 0;
         nbMesuresPositives = 0;
         //Serial.println("Debut2 Err");
       }
     }
   break;
   case SEQ_STATE_ANAYLSE_SIGNAL_HAUT:
     debugMesures[nbMesures] = mesure;
     if ( mesure > SIG175_CUMUL_SEUIL)
     {
       nbMesuresPositives++;
     }
     nbMesures++;
     if ( nbMesures >= 5 )   /* 1s */
     {
       //timestamps = millis();
       lastDecodedSignal[signalCpt] = nbMesuresPositives ;
       
       //Serial.print(timestamps);
       //Serial.print(" ");
       //for (int dbg=0;dbg<5;dbg++)
       //  {
       //    Serial.print(debugMesures[dbg]);
       //    Serial.print(" ");
       //  }
       //Serial.println();

       sequenceState = SEQ_STATE_ANAYLSE_SIGNAL_BAS;
       nbMesures = 0;
       nbMesuresPositives = 0;
     }
   break;
   case SEQ_STATE_ANAYLSE_SIGNAL_BAS:
     if ( mesure > SIG175_CUMUL_SEUIL)
     {
       nbMesuresPositives++;
     }
     debugMesures[nbMesures] = mesure;
     nbMesures++;
     // une fois sur deux on a un echantillon en moins car le signal dure 1,5s et on
     //echantionne a 200ms
     if ( nbMesures >= ( 8 - (signalCpt & 0x1)) )   /* 1,4s or 1,6  */
     {
       //timestamps = millis();
       
       if ( nbMesuresPositives <= 2)
       {
         //OK on fait le bit suivant ou on a fini
         signalCpt ++;
         if ( signalCpt >= MAX_SIGNAL )
         {
           //Youpi c'est fini
           sequenceState = SEQ_STATE_SIGNAL_DECODED;
           nbMesures = 0;
           nbMesuresPositives = 0;
         }
         else
         {
           //on fait le bit suivant.
           //on ajoute un delai de 100ms
           //Serial.print(timestamps);
           //Serial.print(" ");
           //for (int dbg=0;dbg<7;dbg++)
           //{
           //  Serial.print(debugMesures[dbg]);
           //  Serial.print(" ");
           //}
           //Serial.println();
         
           //delay(115);  //should be 100. mais on dirait qye ca decalle
           //retDelay += 115; //should be 100. mais on dirait qye ca decalle
           sequenceState = SEQ_STATE_ANAYLSE_SIGNAL_HAUT;
           nbMesures = 0;
           nbMesuresPositives = 0;
         }
       }
       else
       {
         //erreur fausse detection
         //Serial.print(timestamps);
         Serial.print(" Err ");
         Serial.print(signalCpt);
         Serial.println(" ieme bit");
         
         for (int dbg=0;dbg<7;dbg++)
         {
           Serial.print(debugMesures[dbg]);
           Serial.print(" ");
         }
         Serial.println();
         
         
         //on ajoute un delai de 100ms
         //delay(115); 
         //retDelay += 115; //should be 100. mais on dirait qye ca decalle
         sequenceState = SEQ_STATE_ANAYLSE_SIGNAL_HAUT;
         //sequenceState = SEQ_STATE_DETECT_DEBUT_SIGNAL_HAUT;
          
         nbMesures = 0;
         nbMesuresPositives = 0;
         
       }
     }
     
   break;
   defaut:
     Serial.println("Erreur, unknown state.");
   break;
  }

  if ( sequenceState == SEQ_STATE_SIGNAL_DECODED )
  {
    newValue = 1;
    
    sequenceState = SEQ_STATE_DETECT_DEBUT_SIGNAL_HAUT;
    
  }
  
}
